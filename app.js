// test2
function generatePermutations(input) {
  const result = [];

  function permute(str, current = "") {
    if (str.length === 0) {
      result.push(current);
      return;
    }

    for (let i = 0; i < str.length; i++) {
      const nextChar = str[i];
      const remainingChars = str.slice(0, i) + str.slice(i + 1);
      permute(remainingChars, current + nextChar);
    }
  }

  permute(input);
  return result;
}

console.log("generatePermutations", generatePermutations("ad"));

// test 3
function findOddOccurrence(arr) {
  return arr.reduce((result, num) => result ^ num, 0);
}

console.log(findOddOccurrence([1, 2, 2, 3, 3, 3, 4, 3, 3, 3, 2, 2, 1]));

// test4
function countSmileys(arr) {
  const validSmileys = arr.filter((smiley) => /^[:;][-~]?[)D]$/.test(smiley));
  return validSmileys.length;
}

console.log(countSmileys([":)", ";(", ";}", ":-D"]));
